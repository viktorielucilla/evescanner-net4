# README #

I find it strange that BitBucket has a README creator yet Atlassian's commercial "Stash" product doesn't. Regardless, this tool allows for scan sharing. Send thanks to Miniluv for this work.

### What is this repository for? ###

* Scan potential targets, post their information for everyone to see.

### How do I get set up? ###

* Requires Visual Studio 2013, .net 4.0 Client Profile

### Who do I talk to? ###

* Goonfleet Jabber - Viktorie_Lucilla
* E-mail - Viktorie Lucilla <viktorie@rifter.ca>

### Thanks ###
Thanks go to boneytooth_thompkins_isk-chip for looking over the code and providing a HUGE feature request post, and antI gIRl for this fabulous mock-up (https://i.imgur.com/vTEcrFz.png)

### License ###
This project is licensed by default under the LGPL 3.0. If you desire an alternate license, please contact me at my e-mail address above. I've been out of EVE most of the time lately, so, I may not get back to you immediately.